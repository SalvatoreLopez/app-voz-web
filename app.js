let sr = new webkitSpeechRecognition();
sr.lang = "es-MX";
let synth = window.speechSynthesis;
let voices = synth.getVoices();

let mostrarTextoEscuchado = document.getElementById("mostrarTextoEscuchado");
let botonMicrofono = document.getElementById("botonMicrofono");
let patronYoutube = document.getElementById("patronYoutube");
let patronGoogle = document.getElementById("patronGoogle");
let guardarPatronYoutube = document.getElementById("guardarPatronYoutube");
let guardarPatronGoolgle = document.getElementById("guardarPatronGoolgle");

patronYoutube.innerHTML = localStorage.getItem("patronesYoutube");
patronGoogle.innerHTML = localStorage.getItem("patronesGoogle");

sr.onresult = (resultado) => {

   
    let indiceUltimoElemento = resultado.results.length - 1;
    let textoEscuchado = resultado.results[indiceUltimoElemento][0].transcript.toLowerCase();

    let patronesYoutube = localStorage.getItem("patronesYoutube").split(",");

    let patronesGoogle = localStorage.getItem("patronesGoogle").split(",");

    if (textoEscuchado.includes("hola mi nombre es")) {
        mostrarTextoEscuchado.innerHTML += `<p class="mb-0">hola ${textoEscuchado.split("hola mi nombre es")[1]} es un gusto conocerte</p>`;
        // speak("hola "+textoEscuchado.split("hola mi nombre es")[1]+" es un gusto conocerte");
    }

    if (textoEscuchado.includes(patronesYoutube[0])) {
        console.log("buscando en youtube" + textoEscuchado.split(patronesYoutube[0])[1]);
        openURL(1, textoEscuchado.split(patronesYoutube[0])[1]);
        mostrarTextoEscuchado.innerHTML += `<p class="mb-0">buscando en youtube ${textoEscuchado.split(patronesYoutube[0])[1]}</p>`;
        // speak("buscando en youtube"+ textoEscuchado.split(patronesYoutube[0])[1]);
        debugger;

    }
    if (textoEscuchado.includes(patronesYoutube[1])) {
        console.log("buscando en youtube" + textoEscuchado.split(patronesYoutube[1])[1]);
        openURL(1, textoEscuchado.split(patronesYoutube[1])[1]);
        mostrarTextoEscuchado.innerHTML += `<p class="mb-0">buscando en youtube ${textoEscuchado.split(patronesYoutube[1])[1]}</p>`;
        // speak("buscando en youtube"+ textoEscuchado.split(patronesYoutube[0])[1]);
        debugger;
    }
    if (textoEscuchado.includes(patronesGoogle[0])) {
        console.log("buscando en google" + textoEscuchado.split(patronesGoogle[0])[1]);
        openURL(2, textoEscuchado.split(patronesGoogle[0])[1]);
        mostrarTextoEscuchado.innerHTML += `<p class="mb-0">buscando en google ${textoEscuchado.split(patronesGoogle[0])[1]}</p>`;
        // speak("buscando en google"+ textoEscuchado.split(patronesGoogle[0])[1]);
    }

};

function speak(texto) {
    let utterance = new SpeechSynthesisUtterance(texto);
    utterance.voice = voices.filter(
        function (voice) {
            return voice.name == 'Monica';
        })[0];
    window.speechSynthesis.speak(utterance);
}

function openURL(opcion, texto) {
    let nuevaUrl = "";
    switch (opcion) {
        case 1:
            nuevaUrl = "https://www.youtube.com/results?search_query="+ texto;
            break;
        case 2:
            nuevaUrl = "http://www.google.com/search?q="+ texto;
            break;
    }

    window.open(nuevaUrl, "_blank");
   
}

guardarPatronYoutube.onclick = function(){
    localStorage.setItem("patronesYoutube",patronYoutube.value);
}

guardarPatronGoolgle.onclick = function (){
    localStorage.setItem("patronesGoogle",patronGoogle.value);
}

// speak("iniciando");

botonMicrofono.onclick = function(){
    console.log("hola mundo");
    sr.start();
    speak("Escuchando.....     di tu comando de voz");
}
